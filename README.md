# Starbound - Enhanced Tech

## Summary
Enhances basic Starbound Techs, aiming to make them a bit more effective and fun to use.
Last updated for Starbound v. 1.3.1

## Tech Changes
* Distortion Sphere: Speed increased.
* Aqua Sphere: Speed increased.
* Spike Sphere: Speed increased.
* Sonic Sphere: Speed increased, launch velocity increased.
* Dash: Speed increased, duration increased, mid-air dashing enabled.
* Sprint: Speed increased, can use jump techs while sprinting.
* Blink Dash: Distance increased, cooldown reduced, mid-air dashing enabled.
* Air Dash: Renamed to Power Dash. It's an even more powerful version of Dash.
* Pulse Jump: Enables two extra jumps instead of one.
* Multi Jump: Enables five extra jumps instead of three. Also increased the height of the jumps.
* Rocket Jump: Rocket Boost's power increased.
* Wall Jump: Enables two extra jumps instead of one.